<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', config('app.name'))</title>
  {{-- Tell the browser to be responsive to screen width --}}
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  @yield('blank_head')
</head>
<body class="@yield('body_class')">

@yield('blank_body')

@yield('blank_scripts')

</body>
</html>
