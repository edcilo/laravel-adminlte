@extends('layouts.blank')

@section('body_class') hold-transition skin-blue layout-top-nav @endsection

@section('blank_head')
{{-- Bootstrap 3.3.7 --}}
<link rel="stylesheet" href="@adminlte('bower_components/bootstrap/dist/css/bootstrap.min.css')">
{{-- Font Awesome --}}
<link rel="stylesheet" href="@adminlte('bower_components/font-awesome/css/font-awesome.min.css')">
{{-- Ionicons --}}
<link rel="stylesheet" href="@adminlte('bower_components/Ionicons/css/ionicons.min.css')">

@stack('css_plugins')

{{-- Theme style --}}
<link rel="stylesheet" href="@adminlte('dist/css/AdminLTE.min.css')">
{{-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. --}}
<link rel="stylesheet" href="@adminlte('dist/css/skins/_all-skins.min.css')">

@stack('css')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

{{-- Google Font --}}
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
@endsection

@section('blank_body')
    <div class="wrapper">
        @include('layouts.partials.headerTopNav')

        {{-- Content Wrapper. Contains page content --}}
        <div class="content-wrapper">
            <div class="container">
                @yield('content')
            </div>
        </div>
        {{-- /.content-wrapper --}}

        @include('layouts.partials.footer', ['withContainer' => true])
    </div>
    {{-- ./wrapper --}}
@endsection

@section('blank_scripts')
{{-- jQuery 3 --}}
<script src="@adminlte('bower_components/jquery/dist/jquery.min.js')"></script>
{{-- jQuery UI 1.11.4 --}}
<script src="@adminlte('bower_components/jquery-ui/jquery-ui.min.js')"></script>
{{-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --}}
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
{{-- Bootstrap 3.3.7 --}}
<script src="@adminlte('bower_components/bootstrap/dist/js/bootstrap.min.js')"></script>

@stack('js_plugins')

{{-- Slimscroll --}}
<script src="@adminlte('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')"></script>
{{-- FastClick --}}
<script src="@adminlte('bower_components/fastclick/lib/fastclick.js')"></script>
{{-- AdminLTE App --}}
<script src="@adminlte('dist/js/adminlte.min.js')"></script>

@stack('js')

@endsection
