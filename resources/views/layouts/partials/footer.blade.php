<footer class="main-footer">
    @if($withContainer) <div class="container"> @endif
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights reserved.
    @if($withContainer) </div> @endif
</footer>
