<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index2.html', function () {
    return view('index2 ');
});

Route::get('/pages/layout/top-nav.html', function () {
    return view('pages.layouts.topNav');
});

Route::get('/pages/forms/general.html',function() {
    return view('pages.forms.general');
});

Route::get('/pages/forms/editors.html',function() {
    return view('pages.forms.editors');
});

Route::get('/pages/forms/advanced.html',function() {
    return view('pages.forms.advanced');
});
